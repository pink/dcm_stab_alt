#!/usr/bin/python3
import epics
import numpy as np
import time
import threading

def main():
    global eve
    eve = threading.Event()

    ## callback function
    def onValueChange(pvname=None, value=None, host=None, **kws):
        global eve
        eve.set()

    ## PV channels
    pitch_pv = epics.PV("PINK:DCMSTAB:calc:pitch:avg", auto_monitor=True, callback=onValueChange)
    setpoint_pv = epics.PV("PINK:DCMSTAB:pitch:setpoint", auto_monitor=True)
    error_pv = epics.PV("PINK:DCMSTAB:pitch:error", auto_monitor=False)
    pitch_dt_pv = epics.PV("PINK:DCMSTAB:pitch:speed", auto_monitor=False)
    error_dt_pv = epics.PV("PINK:DCMSTAB:pitch:error_speed", auto_monitor=False)
    deadband_pv = epics.PV("PINK:DCMSTAB:pitch:deadband", auto_monitor=True)
    kg_pv = epics.PV("PINK:DCMSTAB:pitch:kg", auto_monitor=True)
    kp_pv = epics.PV("PINK:DCMSTAB:pitch:kp", auto_monitor=True)
    kda_pv = epics.PV("PINK:DCMSTAB:pitch:kda", auto_monitor=True)
    kde_pv = epics.PV("PINK:DCMSTAB:pitch:kde", auto_monitor=True)
    kout_pv = epics.PV("PINK:DCMSTAB:pitch:kout", auto_monitor=False)
    mstep_pv = epics.PV("PINK:DCMSTAB:pitch:maxstep", auto_monitor=True)
    dacout_pv = epics.PV("PINK:DCMSTAB:pitch:dacout", auto_monitor=False)
    mostab_out_pv = epics.PV("PINK:DCMSTAB:pitch:mostab_out", auto_monitor=True)
    enable_pv = epics.PV("PINK:DCMSTAB:pitch:enable", auto_monitor=True)
    enable_rbv_pv = epics.PV("PINK:DCMSTAB:pitch:enable_rbv", auto_monitor=False)
    device_pv = epics.PV("PINK:DCMSTAB:pitch:device", auto_monitor=True)
    piezo_set_pv = epics.PV("MONOY01U112L:Piezo2U1", auto_monitor=True)
    ###mostab_set_pv = epics.PV("EMILEL:Mostab0:setOutput", auto_monitor=True)
    time.sleep(1)

    ## variables
    t_last = time.time()
    pitch_last = pitch_pv.value
    error_last = 0
    dac_res = 10000.0/65536.0
    enable_now = -1

    ## main loop
    time.sleep(2)
    print("DCM Stab Alt. Running...")
    while(True):
        eve.wait()
        t_now=time.time()
        dt = (t_now-t_last)
        t_last=t_now
        # calculations
        pitch_now = pitch_pv.value
        error = setpoint_pv.value-pitch_now

        pitch_dt = (pitch_now-pitch_last)/dt
        error_dt = (error-error_last)/dt

        if np.abs(error) > deadband_pv.value:
            kout = kg_pv.value*(kp_pv.value*error + kda_pv.value*pitch_dt + kde_pv.value*error_dt)
            if np.abs(kout)>np.abs(mstep_pv.value):
                kout = np.sign(kout)*mstep_pv.value
        else:
            kout = 0.0

        piezo_out = (piezo_set_pv.value+kout)
        if piezo_out>10000.0:
            piezo_out=10000.0
        elif piezo_out<0.0:
            piezo_out=0.0

        ###mostab_out = int(mostab_set_pv.value+(kout/dac_res))
        mostab_out=0
        if mostab_out>65535:
            mostab_out=65535
        elif mostab_out<0:
            piezo_out=0

        pitch_last = pitch_now
        error_last = error

        try:
            # update PVs
            error_pv.put(error)
            pitch_dt_pv.put(pitch_dt)
            error_dt_pv.put(error_dt)
            kout_pv.put(kout)


            # update device PVs and put if enable
            if device_pv.value:
                mostab_out_pv.put(mostab_out)
                if(enable_pv.value):
                    mostab_set_pv.put(mostab_out)
            else:
                dacout_pv.put(piezo_out)
                if(enable_pv.value):
                    piezo_set_pv.put(piezo_out)

            if enable_now != enable_pv.value:
                enable_now = enable_pv.value
                enable_rbv_pv.put(enable_now)
        except:
            time.sleep(5)

        eve.clear()
    ## End of loop

## Main call
main()

