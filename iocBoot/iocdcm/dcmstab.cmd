#!../../bin/linux-x86_64/dcm

## You may have to change dcm to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/dcm.dbd"
dcm_registerRecordDeviceDriver pdbbase

## Load record instances
dbLoadRecords("db/dcmstab_calc.db","BL=PINK,DEV=DCMSTAB,PITCH_N=10")
dbLoadRecords("db/dcmstab_template.db","BL=PINK,DEV=DCMSTAB,CH=pitch")
dbLoadRecords("db/dcmstab_template.db","BL=PINK,DEV=DCMSTAB,CH=roll")

cd "${TOP}/iocBoot/${IOC}"

set_savefile_path("/EPICS/autosave")
set_pass0_restoreFile("auto_settings.sav")

iocInit

create_monitor_set("auto_settings.req", 30, "BL=PINK,DEV=DCMSTAB")

## Start any sequence programs
#seq sncxxx,"user=epics"
